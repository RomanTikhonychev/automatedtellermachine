//
//  AccountClass.h
//  AutomatedTellerMachine
//
//  Created by Рома on 12/17/15.
//  Copyright © 2015 RomanTikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountClass : NSObject

//Возвращает баланс на счете public double getBalance();
//Снимает указанную сумму со счета //Возвращает сумму, которая была снята public double withdraw(double amount);

- (NSNumber *)getBalance;
- (NSNumber *)withdraw:(NSNumber *)amount;




@end
