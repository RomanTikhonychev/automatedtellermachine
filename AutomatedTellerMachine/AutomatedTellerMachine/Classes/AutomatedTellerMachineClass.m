//
//  AutomatedTellerMachineClass.m
//  AutomatedTellerMachine
//
//  Created by Рома on 12/17/15.
//  Copyright © 2015 RomanTikhonychev. All rights reserved.
//

#import "AutomatedTellerMachineClass.h"


NSNumber * amountOfMoney = 0;
BOOL noCard = YES;
CardClass *currentCard = nil;


@implementation AutomatedTellerMachineClass


// public class ATM { ...
// //Можно задавать количество денег в банкомате ATM(double moneyInATM){
// ... }//
- (void)setupATMWithMoney:(NSNumber *)moneyValue
{
    amountOfMoney = moneyValue;
}


// // возвразает количестов денег в банкомате public double getMoneyInATM() {
// ... }
- (NSNumber *)getMoneyInATM
{
    return amountOfMoney;
}


// //С вызова данного метода начинается работа с картой
// //Метод принимает карту и пин­код, проверяет пин­код карты и не заблокирована ли она //Если неправильный пин­код или карточка заблокирована, возвращаем false. При этом,
// вызов всех последующих методов у ATM с данной картой должен генерировать исключение
// NoCardInserted
// public boolean validateCard(Card card, int pinCode){ ...
// }
- (BOOL)validateCard:(CardClass *)inputCard withPinCode:(NSInteger *)pinCode
{
    if (![inputCard isBlocked] && [inputCard checkPin:pinCode]) {
        
        currentCard = inputCard;
        return YES;
    }
    return NO;
}


// //Возвращает сколько денег есть на счету public double checkBalance(){
// ... }
- (NSNumber *)checkBalance
{
    if ([self checkIfCardAvailable]) {
        
        NSNumber * balance;
        AccountClass *currentAccount =  [currentCard getAccount];
        balance = [currentAccount getBalance];
        return balance;
    }
    
    NSNumber *returnErrorNumber = [[NSNumber alloc] initWithDouble:-1];
    return returnErrorNumber;
}


// //Метод для снятия указанной суммы
// //Метод возвращает сумму, которая у клиента осталась на счету после снятия //Кроме проверки счета, метод так же должен проверять достаточно ли денег в самом
// банкомате
// //Если недостаточно денег на счете, то должно генерироваться исключение
// NotEnoughMoneyInAccount
// //Если недостаточно денег в банкомате, то должно генерироваться исключение
// NotEnoughMoneyInATM
// ￼
// //При успешном снятии денег, указанная сумма должна списываться со счета, и в банкомате должно уменьшаться количество денег
// public double getCash(double amount){ ...
// } }
- (NSNumber *)getCash:(NSNumber *)amount
{
    NSNumber *returnErrorNumber = [[NSNumber alloc] initWithDouble:-1];
    
    if ([self checkIfCardAvailable]) {
        
        NSNumber *balance;
        AccountClass *currentAccount =  [currentCard getAccount];
        balance = [currentAccount getBalance];
        
        if (amountOfMoney < amount) {
            NSException* exception = [NSException
                                      exceptionWithName:@"NotEnoughMoneyInAccount"
                                      reason:@"Not Enough Money In Account"
                                      userInfo:nil];
            @throw exception;
            return returnErrorNumber;
        }
        else if (balance < amount) {
            
            NSException* exception = [NSException
                                      exceptionWithName:@"NotEnoughMoneyInAccount"
                                      reason:@"Not Enough Money In Account"
                                      userInfo:nil];
            @throw exception;
            return returnErrorNumber;
        }
        else
        {
            
            [currentAccount withdraw:amount];
            balance = [NSNumber numberWithFloat: [balance floatValue] - [amount floatValue] ];
            return balance;
        }
        
    }
    return returnErrorNumber;
}

#pragma mark - AdditionalMethods

- (BOOL)checkIfCardAvailable
{
    if (noCard) {
        
        NSException* exception = [NSException
                                  exceptionWithName:@"NoCardInserted"
                                  reason:@"There is no card in ATM"
                                  userInfo:nil];
        @throw exception;
        
        return NO;
    }
    return YES;
}

@end






