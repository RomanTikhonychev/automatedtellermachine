//
//  CardClass.h
//  AutomatedTellerMachine
//
//  Created by Рома on 12/17/15.
//  Copyright © 2015 RomanTikhonychev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AccountClass.h"

@interface CardClass : NSObject

//Заблокирована ли карта или нет public boolean isBlocked();
//Возвращает счет связанный с данной картой public Account getAccount();
//Проверяет корректность пин­кода public boolean checkPin(int pinCode);


- (BOOL)isBlocked;
- (AccountClass *)getAccount;
- (BOOL)checkPin:(NSInteger *)pinCode;

@end
